# Youcoub
Youcoub is a service for automatic videos generation for your YouTube channel from coub.com.  
After the installation on the machine where the installer was running, a docker container will appear with connected data and logs directories. Depending on the different configurations of _install.conf_, periodically the youcoub application in the container will generate clips for your channel.
## Features:
- Scheduled launch
- Flexible configuration, including memory consumption
- Asynchronous download and video convertation
- Periodic saving of states during the creation of a clip, including for each individual cube in a save file
- Sending letters about successful uploading video or errors with logs

## Dependencies
  * [python 2.7](https://www.python.org/downloads/)
  * [ansible](http://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
  * [docker server](https://www.docker.com/community-edition#/download)
  * [docker-py](https://github.com/docker/docker-py)

## Installation (Ubuntu)
1) Install dependencies:

```shell
$ sudo su
$ apt update && apt -y install python ansible docker.io python-pip
$ pip install docker
```
2) Create empty directory somewhere and put your video artifacts there:

```shell
/some/data/directory/
├── startsaver.mp4
├── padding.mp4
├── endsaver.mp4
└── logo.png
```

where:
- **startsaver** - the video to be shown at the beginning of each clip 
- **padding** - the video to be shown between coubs, this video should be small in duration, for example a small padding
- **endsaver** - the video to be shown after clip, in this part you can offer to subscribe to your channel and watch your other videos
- **logo** - your channel or other logo image, which will be placed on the right side of thumbnail image

> You can create an empty directory or you may not have some videos, it is optional

3) Create another empty directory somewhere for logging files, _ex. /some/logs/directory/_  
4) Add your current user to the docker group - https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user  
5) Get your authorization credentials json file -  https://developers.google.com/youtube/registering_an_application  
Put this json file into your data directory from second step and rename it to **client_secret.json**  
6) Customize your _install.conf_ configuration file:
- **localhost ansible_connection=local ansible_become_pass=password** - in ansible_become_pass=\<your current user SUDO password\>
- **[target]**  
  **docker_channel_name** - name of local docker container, which will be created and where youcoub will be installed.
- **name** - your YouTube channel name, _ex. My Best Coubs!_
- **slogan** - your slogan or advice, _ex. Cool Jokes!_
- **data_directory** - absolute path to your data directory from second step
- **logs_directory** - absolute path to your logs directory from third step
- **start_saver_video** - name of your startsaver file in your **data_directory**, may be empty
- **padding_video** - name of your padding file in your **data_directory**, may be empty
- **end_saver_video** - name of your padding file in your **data_directory**, may be empty
- **logo** - name of your logo file in your **data_directory**, may be empty
- **clips_title_pattern** - patthern for your clips title, this is string where you can use:
    - **{NUMBER}** - number of current clip
    - **{{ name }}** - your channel name from this config
    - **{{ slogan }}** - your slogan from this config
    - _all other variables, even new_
- **clips_width** - clips width resolution
- **clips_height** - clips height resolution
- **clips_description** - your clips description pattern, may contain **{COUBS_DESCRIPTION}** - numerated coubs description in current clip, with music, if they have. You can also use all other variables if you want, for example **{{ name }}** or **{{ slogan }}**
- **clips_language** - two-letter description of clips language
- **clips_tags** - tags of your clips in youtube
- **main_thumb_font** - font for your **{{ name }}** and **{{ slogan }}** in clips thumbnail
- **secondary_thumb_font** - font for clips number in thumbnail
- **limit_memory** - limit your docker container memory, take a positive integer, followed by a suffix of b, k, m, g, to indicate bytes, kilobytes, megabytes, or gigabytes
- **limit_cpus** - maximum number of cpu of docker container
- **email_notification** - your email where you will receive upload or error notifications with logs
> Additional options, you may not change it
- **ansible_connection** - docker
- **credentials_oauth2** - filename of authorization credentials json file in your **data_directory**
- **credentials_token** - filename of authorization credentials json file cache in your **data_directory** (it will be appear after authorization, when first clip will make)
- **order_by** - coub.com api variable for coubs search
- **ban_not_funny** - do not add not funny coubs in clip - coubs which don't have _funny_ coub.com tag
- **one_coub_sec_length** - one coub maximum duration in seconds
- **days_depth** - how old coubs may be in your clips, since creation date
- **get_coubs** - number of coubs in your clip
- **start_clip_number** - if your channel already have other clips, you can set any number for fisrt clip
- **crontab_minute**  
**crontab_hour**  
**crontab_weekday** - setup your clips generation date/time  

7) Run ```install.sh``` in cloned repository:

```shell
$ bash ./install.sh
```
    
For the first time, you need to allow this youcoub application to access your YouTube api. Open link from console in your browser, if it don't opened automatically and then copy code from browser back to console and press _Enter_. If your **data_directory** will be appear **credentials_token** for clips uploading and thumbnail setting.  
Also there will appear **channel.save** file which contain all clips and your current config.
 
Final possible **data_directory** after installation and clip making:

```shell
/some/data/directory/
├── channel.save
├── client_secret.json
├── startsaver.mp4
├── padding.mp4
├── endsaver.mp4
├── logo.png
└── token.json
```

## Tips and tricks
### Adding fonts
You may want to use any custom font in youcoub.
To view already existing fonts on your docker container, type:

```shell
$ docker exec -it docker_channel_name fc-list
```

To add custom font files to this container, type:
```shell
$ FONT=/local/path/to/font.ttf; docker cp $FONT docker_channel_name:/usr/local/share/fonts/$(basename $FONT) && docker exec docker_channel_name fc-cache -f
```

> Remember to right typing **main_thumb_font** and **secondary_thumb_font** - font name must containing ```-``` instead of spaces, ex. "Astakhov-First-Simple" instead of "Astakhov First Simple"

### Updating config or pulling git changes
After updating _install.conf_ or pulling changes from remote repository, type:

```shell
$ bash ./install.sh
```

### Backup channel
If you want to save or move your youcoub instance to another host, copy all **data_directory**, especially **channel.save**

### Examples
- https://www.youtube.com/channel/UCyqyWt_FSoBfGN8YMl9Srow
- https://www.youtube.com/channel/UCn5jGAxKf3s5_puZHx4FeBw
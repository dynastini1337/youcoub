#!/usr/bin/env bash
#
# Note: Configure install.conf first!

### READ UTILS ###
YOUCOUB_INSTALLER_PATH=$(dirname $0)
source $YOUCOUB_INSTALLER_PATH/utils.sh
trap 'trap_error ${LINENO}' ERR

### MAIN SCRIPT ###

# Check configuration existence
if [ ! -e $YOUCOUB_INSTALLER_PATH/install.conf ]; then
    err "Can't find install.conf file"
fi

# Run ansible playbook
info "Start youcube installing with $YOUCOUB_INSTALLER_PATH/install.conf configuration"
mkdir -p $YOUCOUB_INSTALLER_PATH/logs
export ANSIBLE_STDOUT_CALLBACK=yaml
export ANSIBLE_FORCE_COLOR=True
export ANSIBLE_HOST_KEY_CHECKING=False
export ANSIBLE_LOCAL_TEMP=$HOME/.ansible/tmp
export ANSIBLE_REMOTE_TEMP=$HOME/.ansible/tmp
set -o pipefail
LOGPATH=$(sed -n 's/\ *logs_directory\ *=\ *\(.*\)\ *$/\1/p' $YOUCOUB_INSTALLER_PATH/install.conf)/$(date '+%Y-%m-%d_%H:%M:%S').install.log
info "Create $LOGPATH file"
mkdir -p $(dirname $LOGPATH)
echo -e "To correctly view this log file with colors, use command:\n    $ less -R $LOGPATH\n\n" > $LOGPATH

# Recreate docker container
info "Overwrite (if any) youcoub docker instance"
ansible-playbook -i $YOUCOUB_INSTALLER_PATH/install.conf \
$YOUCOUB_INSTALLER_PATH/data/installer/create_container.yml -v 2>&1 | tee -a $LOGPATH

# Get Youtube token if it doesn't exist
YOUTUBE_DATA_DIR=$(sed -n 's/\ *data_directory\ *=\ *\(.*\)\ *$/\1/p' $YOUCOUB_INSTALLER_PATH/install.conf )
YOUTUBE_CS=$(sed -n 's/\ *credentials_oauth2\ *=\ *\(.*\)\ *$/\1/p' $YOUCOUB_INSTALLER_PATH/install.conf )
YOUTUBE_TOKEN=$(sed -n 's/\ *credentials_token\ *=\ *\(.*\)\ *$/\1/p' $YOUCOUB_INSTALLER_PATH/install.conf )
info "Check for existence $YOUTUBE_TOKEN in $YOUTUBE_DATA_DIR"
if [ ! -e "$YOUTUBE_DATA_DIR/$YOUTUBE_TOKEN" ]; then
    wrn "Can't find $YOUTUBE_DATA_DIR/$YOUTUBE_TOKEN, building auth tool"
    ansible-playbook -i $YOUCOUB_INSTALLER_PATH/install.conf \
    $YOUCOUB_INSTALLER_PATH/data/installer/auth.yml -v 2>&1 | tee -a $LOGPATH
    info "Get oauth refresh token for youtube"
    dbg "/tmp/youtube_auth -secrets=$YOUTUBE_DATA_DIR/$YOUTUBE_CS -cache=$YOUTUBE_DATA_DIR/$YOUTUBE_TOKEN"
    /tmp/youtube_auth -secrets=$YOUTUBE_DATA_DIR/$YOUTUBE_CS -cache=$YOUTUBE_DATA_DIR/$YOUTUBE_TOKEN
fi

# Install or update your targets
trap - ERR
info "Continue installation to targets"
ansible-playbook -i $YOUCOUB_INSTALLER_PATH/install.conf \
$YOUCOUB_INSTALLER_PATH/data/installer/main.yml -v 2>&1 | tee -a $LOGPATH

# Finishing
if [ $? -ne 0 ]; then
    err "Failed to install: check output or $LOGPATH for more information"
fi
info "Installation succeeded"
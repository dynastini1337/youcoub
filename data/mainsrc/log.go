package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"sync"
	"time"
)

// Logging levels
var (
	I  *log.Logger
	W  *log.Logger
	E  *log.Logger
	IF *log.Logger
	WF *log.Logger
	EF *log.Logger
)

// RotateWriter is file rotation logger
type RotateWriter struct {
	lock     sync.Mutex
	filename string // should be set to the actual filename
	fp       *os.File
}

// NewRotateWriter creates new RotateWriter. Return nil if error occurs during setup.
func NewRotateWriter(filename string) *RotateWriter {
	w := &RotateWriter{filename: filename}
	err := w.Rotate()
	if err != nil {
		return nil
	}
	return w
}

// Write satisfies the io.Writer interface.
func (w *RotateWriter) Write(output []byte) (int, error) {
	w.lock.Lock()
	defer w.lock.Unlock()
	return w.fp.Write(output)
}

// Rotate is perform the actual act of rotating and reopening file.
func (w *RotateWriter) Rotate() (err error) {
	w.lock.Lock()
	defer w.lock.Unlock()

	// Close existing file if open
	if w.fp != nil {
		err = w.fp.Close()
		w.fp = nil
		if err != nil {
			return
		}
	}
	// Rename dest file if it already exists
	_, err = os.Stat(w.filename)
	if err == nil {
		err = os.Rename(w.filename, w.filename+"."+time.Now().Format(time.RFC3339))
		if err != nil {
			return
		}
	}

	// Create a file.
	w.fp, err = os.Create(w.filename)
	return
}

// LogInit doing logging handlers initilization
func LogInit(logDir string) {
	err := os.MkdirAll(logDir, os.ModePerm)
	if err != nil {
		fmt.Println("Can't create logs directory")
		os.Exit(-1)
	}

	rw := NewRotateWriter(logDir + "/youcoub.log")
	I = log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime)
	W = log.New(os.Stdout, "WARNING: ", log.Ldate|log.Ltime)
	E = log.New(os.Stderr, "ERROR: ", log.Ldate|log.Ltime)
	IF = log.New(rw, "INFO: ", log.Ldate|log.Ltime)
	WF = log.New(rw, "WARNING: ", log.Ldate|log.Ltime)
	EF = log.New(rw, "ERROR: ", log.Ldate|log.Ltime)
}

func logPrint(level int, logger *log.Logger, v ...interface{}) {
	_, file, line, ok := runtime.Caller(level)
	if !ok {
		file = "???"
		line = 0
	} else {
		file = filepath.Base(file)
	}

	logger.Print(fmt.Sprintf("%s:%d ", file, line), fmt.Sprintln(v...))
}

// Info printing result to stdout
func Info(v ...interface{}) {
	logPrint(2, I, v...)
	logPrint(2, IF, v...)
}

// InfoL printing result to stdout with given runtime level
func InfoL(level int, v ...interface{}) {
	logPrint(level, I, v...)
	logPrint(level, IF, v...)
}

// Warning printing result to stdout
func Warning(v ...interface{}) {
	logPrint(2, W, v...)
	logPrint(2, WF, v...)
}

// Error printing result to stderr
func Error(v ...interface{}) {
	logPrint(2, E, v...)
	logPrint(2, EF, v...)
	// os.Exit(1)
}

// ErrorL printing result to stderr with given runtime level
func ErrorL(level int, v ...interface{}) {
	logPrint(level, E, v...)
	logPrint(level, EF, v...)
	// os.Exit(1)
}

// Fatal printing result to stderr and terminated
func Fatal(v ...interface{}) {
	logPrint(2, E, v...)
	logPrint(2, EF, v...)
	os.Exit(1)
}

// FatalL printing result to stderr with given runtime level and terminated
func FatalL(level int, v ...interface{}) {
	logPrint(level, E, v...)
	logPrint(level, EF, v...)
	os.Exit(1)
}

// Sample Go code for user authorization

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"

	"golang.org/x/oauth2"
	"google.golang.org/api/youtube/v3"
)

// ClientConfig is a data structure definition for the client_secrets.json file.
// The code unmarshals the JSON configuration file into this structure.
type ClientConfig struct {
	ClientID     string   `json:"client_id"`
	ClientSecret string   `json:"client_secret"`
	RedirectURIs []string `json:"redirect_uris"`
	AuthURI      string   `json:"auth_uri"`
	TokenURI     string   `json:"token_uri"`
}

// OAuthConfig is a root-level configuration object.
type OAuthConfig struct {
	Installed ClientConfig `json:"installed"`
	Web       ClientConfig `json:"web"`
}

// readConfig reads the configuration from clientSecretsFile.
// It returns an oauth configuration object for use with the Google API client.
func readConfig(clientSecretsFile, scope string) (*oauth2.Config, error) {
	// Read the secrets file
	data, err := ioutil.ReadFile(clientSecretsFile)
	if err != nil {
		return nil, err
	}

	cfg := new(OAuthConfig)
	err = json.Unmarshal(data, &cfg)
	if err != nil {
		return nil, err
	}

	var redirectURI string
	if len(cfg.Web.RedirectURIs) > 0 {
		redirectURI = cfg.Web.RedirectURIs[0]
	} else if len(cfg.Installed.RedirectURIs) > 0 {
		redirectURI = cfg.Installed.RedirectURIs[0]
	} else {
		return nil, errors.New("Must specify a redirect URI in config file or when creating OAuth client")
	}

	return &oauth2.Config{
		ClientID:     cfg.Installed.ClientID,
		ClientSecret: cfg.Installed.ClientSecret,
		Scopes:       []string{scope},
		Endpoint:     oauth2.Endpoint{cfg.Installed.AuthURI, cfg.Installed.TokenURI},
		RedirectURL:  redirectURI,
	}, nil
}

func buildOAuthHTTPClient(clientSecretsFile, cacheFile, scope string) (*http.Client, error) {
	config, err := readConfig(clientSecretsFile, scope)
	if err != nil {
		return nil, err
	}

	ctx := oauth2.NoContext
	var token *oauth2.Token

	data, err := ioutil.ReadFile(cacheFile)
	if err == nil {
		err = json.Unmarshal(data, &token)
		if err != nil {
			return nil, err
		}
	}

	tokenSource := config.TokenSource(oauth2.NoContext, token)
	newToken, err := tokenSource.Token()
	if err != nil {
		return nil, err
	}

	data, err = json.Marshal(newToken)
	if err != nil {
		return nil, err
	}
	ioutil.WriteFile(cacheFile, data, 0644)
	Info("Saved new token:", newToken.AccessToken)

	return oauth2.NewClient(ctx, tokenSource), nil
}

func addPropertyToResource(ref map[string]interface{}, keys []string, value string, count int) map[string]interface{} {
	for k := count; k < (len(keys) - 1); k++ {
		switch val := ref[keys[k]].(type) {
		case map[string]interface{}:
			ref[keys[k]] = addPropertyToResource(val, keys, value, (k + 1))
		case nil:
			next := make(map[string]interface{})
			ref[keys[k]] = addPropertyToResource(next, keys, value, (k + 1))
		}
	}
	// Only include properties that have values.
	if count == len(keys)-1 && value != "" {
		valueKey := keys[len(keys)-1]
		if valueKey[len(valueKey)-2:] == "[]" {
			ref[valueKey[0:len(valueKey)-2]] = strings.Split(value, ",")
		} else if len(valueKey) > 4 && valueKey[len(valueKey)-4:] == "|int" {
			ref[valueKey[0:len(valueKey)-4]], _ = strconv.Atoi(value)
		} else if value == "true" {
			ref[valueKey] = true
		} else if value == "false" {
			ref[valueKey] = false
		} else {
			ref[valueKey] = value
		}
	}
	return ref
}

func createResource(properties map[string]string) string {
	resource := make(map[string]interface{})
	for key, value := range properties {
		keys := strings.Split(key, ".")
		ref := addPropertyToResource(resource, keys, value, 0)
		resource = ref
	}
	propJSON, err := json.Marshal(resource)
	if err != nil {
		Error(err)
	}
	return string(propJSON)
}

func videoInsert(service *youtube.Service, part string, res string, filename string) string {
	resource := &youtube.Video{}
	if err := json.NewDecoder(strings.NewReader(res)).Decode(&resource); err != nil {
		Error(err)
	}
	call := service.Videos.Insert(part, resource)

	file, err := os.Open(filename)
	defer file.Close()
	if err != nil {
		Error(err)
	}

	response, err := call.Media(file).Do()
	if err != nil {
		Error(err)
	}

	return response.Id
}

func thumbnailsSet(service *youtube.Service, videoId string, filename string) {
	call := service.Thumbnails.Set(videoId)

	file, err := os.Open(filename)
	defer file.Close()
	if err != nil {
		Error(err)
	}

	_, err = call.Media(file).Do()
	if err != nil {
		Error(err)
	}
}

// UploadClip upload clip to youtube channel and then setup generated thumbnail
func UploadClip(config *Config, clip *Clip, clippath string, thumbnailpath string) {
	client, err := buildOAuthHTTPClient(
		config.CredentialsOAuth2,
		config.CredentialsToken,
		youtube.YoutubeUploadScope)
	if err != nil {
		Error("Error building OAuth client", err)
	}

	service, err := youtube.New(client)
	if err != nil {
		Error("Error creating YouTube client", err)
	}

	properties := (map[string]string{
		"snippet.categoryId":      "23", // Humor id, because coubs are pretty funny, duh?
		"snippet.defaultLanguage": config.ClipsLanguage,
		"snippet.description":     strings.Replace(config.ClipsDescription, "{COUBS_DESCRIPTION}", clip.Description, -1),
		"snippet.tags[]":          strings.Replace(config.ClipsTags, "{CLIP_TAGS}", clip.Tags, -1),
		"snippet.title":           clip.Title,
		"status.privacyStatus":    "private",
	})
	res := createResource(properties)

	videoID := videoInsert(service, "snippet,status", res, clippath)
	thumbnailsSet(service, videoID, thumbnailpath)

	err = os.RemoveAll(config.UnfinishedPath)
	if err != nil {
		Error(fmt.Sprintf("can't move %v tmp directory", config.UnfinishedPath))
	}
}

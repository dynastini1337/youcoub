// 1. Parse best coubs from https://coub.com/
// 2. Generate video file from parsed coubs
// 3. Upload to https://youtube.com

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

// Name of save file
const sf = "channel.save"

func main() {
	// Read config file and/or commandline arguments
	config := Config{}
	config.Name = *flag.String("name", "", "Name of youtube channel")
	config.Slogan = *flag.String("slogan", "", "Slogan of youtube channel")
	config.StartSaverVideo = *flag.String("startsaver_video", "", "Path to padding video between coubs")
	config.PaddingVideo = *flag.String("padding_video", "", "Path to padding video between coubs")
	config.EndSaverVideo = *flag.String("endsaver_video", "", "Path to padding video between coubs")
	config.ClipsTitlePattern = *flag.String("name_pattern", "Test Video", "Pattern for clips title")
	config.ClipsWidth = *flag.Uint64("clips_width", 1920, "Clips width")
	config.ClipsHeight = *flag.Uint64("clips_height", 1080, "Clips height")
	config.ClipsDescription = *flag.String("description", "Test Description", "This description will be added for each clip")
	config.ClipsLanguage = *flag.String("language", "en", "Clips language")
	config.ClipsTags = *flag.String("tags", "youcoub, humor", "This tags will be set for each clip")
	config.MainThumbFont = *flag.String("main_font", "", "Font for channel name and slogan")
	config.SecondaryThumbFont = *flag.String("secondary_font", "", "Font for clip number")
	config.EmailNotification = *flag.String("email_notification", "", "Email address for upload or errors notifications")
	config.SavesPath = *flag.String("saves_path", "saves", "Path to save files directory")
	config.LogsPath = *flag.String("logs_path", "logs", "Path to log files directory")
	config.UnfinishedPath = *flag.String("unfinished_path", "unfinished", "Path to unfinished files directory")
	config.CredentialsOAuth2 = *flag.String("oauth2_json", "", "Youtube API credentials json file")
	config.CredentialsToken = *flag.String("token_json", "", "Youtube API token file")
	config.Logo = *flag.String("logo", "", "Path to logo image file")
	config.OrderBy = *flag.String("order_by", "oldest", "Coub search strategy")
	config.BanNotFunny = *flag.Bool("ban_not_funny", false, "Don't use not funny coubs")
	config.OneCoubSecLength = *flag.Uint64("coub_length", 10, "One coub duration time in seconds")
	config.DaysDepth = *flag.Uint64("days_depth", 10, "Don't search coubs older than this number of days")
	config.GetCoubs = *flag.Uint64("coubs_num", 45, "Coub number is final clip")
	config.StartClipNumber = *flag.Uint64("start_clip_number", 1, "First clip number")
	configFile := flag.String("config", "", "Absolute of relative path to configuration file")
	flag.Parse()

	// Read configFile configuration file
	cf := *configFile
	if cf != "" {
		file, err := os.Open(cf)
		defer file.Close()
		if err != nil {
			os.Exit(-1)
		}

		decoder := json.NewDecoder(file)
		err = decoder.Decode(&config)
		if err != nil {
			fmt.Println(err)
			os.Exit(-1)
		}
	}

	LogInit(config.LogsPath)

	// Create or read youtube channel saved file
	var Yc YoutubeChannel

	sfPath := fmt.Sprintf("%v/%v", config.SavesPath, sf)

	err := LoadChannel(sfPath, &Yc)
	if err != nil {
		Warning(err)
		Yc = YoutubeChannel{}
		Info(fmt.Sprintf("Creating new saved file: %v", sf))
	} else {
		Info(fmt.Sprintf("Loading saved file: %v", sf))
	}
	Yc.SavePath = sfPath
	Yc.Config = config
	Yc.Save()

	Info(fmt.Sprintf("Youtube channel options: %#v", Yc.Config))

	// Creating new or resuming existing clip processing
	var Nc *Clip

	if Yc.Clips != nil {
		_, err := NextState(Yc.Clips[len(Yc.Clips)-1].State, ClipStates)
		if err == nil {
			Nc = &Yc.Clips[len(Yc.Clips)-1]
			Info(fmt.Sprintf("Continue with last unfinished clip number %v", Nc.No))
		} else {
			Nc = Yc.NewClip()
		}
	} else {
		Nc = Yc.NewClip()
	}

	Yc.Proceed()
}

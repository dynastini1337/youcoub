package main

import (
	"bytes"
	"fmt"
	"math"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
)

// BashRun used to run any bash command and get stdout, stderr result
func BashRun(cmdstr string) (string, error) {
	cmd := exec.Command("bash", "-c", cmdstr)
	InfoL(4, fmt.Sprintf("run cmd: %v", cmdstr))
	var out, stderr bytes.Buffer
	cmd.Stdout, cmd.Stderr = &out, &stderr
	err := cmd.Run()
	if err != nil {
		return "", fmt.Errorf("%v: %v", err, string(stderr.Bytes()))
	}
	return strings.TrimSuffix(out.String(), "\n"), nil
}

// FFprobeRun run ffprobe command
func FFprobeRun(filePath, cmdstr string) string {
	binary, err := exec.LookPath("ffprobe")
	if err != nil {
		Error(err)
	}
	resArgs := []string{binary}

	resArgs = append(resArgs, "-i "+filePath)
	resArgs = append(resArgs, cmdstr)

	runCmd := strings.Join(resArgs[:], " ")
	out, err := BashRun(runCmd)
	if err != nil {
		Error(fmt.Sprintf("run cmd: %v", runCmd), err)
	}
	return string(out)
}

// FFmpegScale scales video relotution to (wTarget)x(hTarget), set sar=1 with resolution deviation (0-100%)
func FFmpegScale(videoPath string, wTarget, hTarget, deviation uint64) (uint64, uint64, error) {
	Size := FFmpegRun(false, false, false, videoPath, "2>&1 | grep -oP 'Stream .*, \\K[0-9]+x[0-9]+'")
	splitCropSize := strings.Split(Size, "x")
	sw, errw := strconv.ParseInt(splitCropSize[0], 10, 64)
	if errw != nil {
		Error("can't convert width to uint64")
	}
	sh, errh := strconv.ParseInt(splitCropSize[1], 10, 64)
	if errh != nil {
		Error("can't convert height to uint64")
	}
	w, h := uint64(sw), uint64(sh)

	var resx, resy uint64
	if w == wTarget || h == hTarget {
		resx, resy = w, h
	} else {
		kx, ky := float64(wTarget)/float64(w), float64(hTarget)/float64(h)

		if kx >= ky {
			resy = hTarget
			tmp := uint64(math.Round(ky * float64(w)))
			if tmp >= wTarget*deviation/100 {
				resx = wTarget
			} else {
				resx = tmp
				if tmp%2 != 0 {
					resx--
				}
			}
		} else {
			resx = wTarget
			tmp := uint64(math.Round(kx * float64(w)))
			if tmp >= hTarget*deviation/100 {
				resy = hTarget
			} else {
				resy = tmp
				if tmp%2 != 0 {
					resy--
				}
			}
		}

		FFmpegRun(true, true, false, videoPath, fmt.Sprintf("-strict -2 -vf 'scale=%v:%v,setsar=1'", resx, resy))
	}

	Info(fmt.Sprintf("scaled %v to %vx%v border", videoPath, resx, resy))

	return resx, resy, nil
}

// FFmpegRun run ffmpeg command
func FFmpegRun(silent bool, newfile bool, noinput bool, filePath, cmdstr string) string {
	binary, err := exec.LookPath("ffmpeg")
	if err != nil {
		Fatal(err)
	}
	resArgs := []string{binary}

	if silent {
		resArgs = append(resArgs, "-loglevel quiet")
	}
	resArgs = append(resArgs, "-y")
	if !noinput {
		resArgs = append(resArgs, "-i "+filePath)
	}
	resArgs = append(resArgs, cmdstr)

	if newfile {
		newCoubPath := strings.TrimSuffix(filePath, ".mp4") + "_new.mp4"
		resArgs = append(resArgs, newCoubPath)
		// resArgs = append(resArgs, "-crf 18 -preset veryslow "+newCoubPath)
		defer func() {
			err = os.Rename(newCoubPath, filePath)
			if err != nil {
				Fatal(fmt.Sprintf("can't move coub to %v", newCoubPath))
			}
		}()
	}

	runCmd := strings.Join(resArgs[:], " ")
	out, err := BashRun(runCmd)
	if err != nil {
		Fatal(fmt.Sprintf("run cmd: %v", runCmd), err)
	}
	return string(out)
}

// FFprobeRun run ffprobe command for current coub
func (coub *Coub) FFprobeRun(cmdstr string) string {
	return FFprobeRun(fmt.Sprintf("%v/%v.mp4", coub.UnfinishedPath, coub.No), cmdstr)
}

// FFmpegRun run ffmpeg command for current coub
func (coub *Coub) FFmpegRun(silent bool, newfile bool, noinput bool, cmdstr string) string {
	return FFmpegRun(silent, newfile, noinput, fmt.Sprintf("%v/%v.mp4", coub.UnfinishedPath, coub.No), cmdstr)
}

func (coub *Coub) ffmpegCrop() error {
	if coub.State != "Downloaded" {
		return nil
	}

	rd := coub.FFprobeRun("-show_entries format=duration -v quiet -of csv=\"p=0\"")
	rawduration, err := strconv.ParseFloat(rd, 64)
	if err != nil {
		return err
	}
	coub.RawDuration = rawduration

	cropsCh := make(chan string)
	cropsWg := new(sync.WaitGroup)
	for i := uint64(0); i < uint64(coub.RawDuration); i++ {
		cropsWg.Add(1)
		go func(i uint64) {
			cropsCh <- coub.FFmpegRun(false, false, false,
				fmt.Sprintf("-ss %v -t 1 -vf cropdetect=20:2:0 -f null - 2>&1 | awk -F 'crop=' '/crop/ {print $NF}' | sort -n | tail -1", i))
			cropsWg.Done()
		}(i)
	}

	go func() {
		cropsWg.Wait()
		close(cropsCh)
	}()

	actualSize := coub.FFmpegRun(false, false, false, "2>&1 | grep -oP 'Stream .*, \\K[0-9]+x[0-9]+'")

	var w, h uint64
	var cropSize string
	for c := range cropsCh {
		nextw, nexth := cropToUint64(c)
		if nextw*nexth > w*h {
			w, h, cropSize = nextw, nexth, c
		}
	}
	coub.Info("size:", actualSize, "crop:", cropSize)

	if actualSize != fmt.Sprintf("%vx%v", w, h) {
		coub.FFmpegRun(true, true, false, fmt.Sprintf("-vf \"crop=%v\"", cropSize))
		coub.Info(fmt.Sprintf("coub was cropped to %v", cropSize))
	}
	coub.RawWidth, coub.RawHeight = w, h
	coub.Info(fmt.Sprintf("raw size %vx%v", coub.RawWidth, coub.RawHeight))

	coub.nextState()
	return nil
}

func (coub *Coub) ffmpegScale(wTarget, hTarget uint64) error {
	if coub.State != "FFmpeg_cropped" {
		return nil
	}

	w, h, err := FFmpegScale(fmt.Sprintf("%v/%v.mp4", coub.UnfinishedPath, coub.No), wTarget, hTarget, 99)
	if err != nil {
		return err
	}

	coub.ScaledWidth, coub.ScaledHeight = w, h
	coub.nextState()
	return nil
}

func (coub *Coub) ffmpegBackground(wTarget, hTarget uint64) error {
	if coub.State != "FFmpeg_scaled" {
		return nil
	}

	coub.Info(fmt.Sprintf("scaled size %vx%v", coub.ScaledWidth, coub.ScaledHeight))

	if coub.ScaledWidth != wTarget && coub.ScaledHeight != hTarget {
		return fmt.Errorf("bad size %vx%v", coub.ScaledWidth, coub.ScaledHeight)
	}

	if coub.ScaledWidth < wTarget {
		coub.FFmpegRun(true, true, false,
			fmt.Sprintf("-lavfi '[0:v]scale=%v:-1,boxblur=luma_radius=60:luma_power=3:chroma_radius=60:chroma_power=3[bg];"+
				"[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=h=%v,setsar=1'", wTarget, hTarget))
		coub.Info("backgrounded vertically")
	}
	if coub.ScaledHeight < hTarget {
		coub.FFmpegRun(false, true, false,
			fmt.Sprintf("-lavfi '[0:v]scale=-1:%v,boxblur=luma_radius=60:luma_power=3:chroma_radius=60:chroma_power=3[bg];"+
				"[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=w=%v,setsar=1'", hTarget, wTarget))
		coub.Info("backgrounded horizontally")
	}

	coub.nextState()
	return nil
}

func (coub *Coub) ffmpegMultiply(maxLen uint64) error {
	if coub.State != "FFmpeg_backgrounded" {
		return nil
	}

	coub.Info(fmt.Sprintf("current duration: %v", coub.RawDuration))
	if coub.RawDuration < float64(maxLen)*0.4 {
		mul := uint64(math.Round(float64(maxLen) * 0.6 / coub.RawDuration))
		if mul == 1 {
			mul++
		}
		fp, err := filepath.Abs(fmt.Sprintf("%v/%v.mp4", coub.UnfinishedPath, coub.No))
		if err != nil {
			return err
		}
		coub.FFmpegRun(true, true, true,
			fmt.Sprintf("-safe 0 -f concat -i <(for i in {1..%v}; do printf \"file '%%s'\\n\" %v; done)", mul, fp))
		coub.Duration = coub.RawDuration * float64(mul)
		coub.Info(fmt.Sprintf("multiplied %v times (duration %v -> %v)", mul, coub.RawDuration, coub.Duration))
	} else if coub.RawDuration > float64(maxLen)+1 {
		coub.FFmpegRun(true, true, false, fmt.Sprintf("-t %v", maxLen))
		coub.Duration = float64(maxLen)
		coub.Info(fmt.Sprintf("cut video (duration %v -> %v)", coub.RawDuration, coub.Duration))
	} else {
		coub.Duration = coub.RawDuration
	}

	coub.nextState()
	return nil
}

func (coub *Coub) ffmpegSound() error {
	if coub.State != "FFmpeg_multiplied" {
		return nil
	}
	defer func() {
		soundPath := fmt.Sprintf("%v/%v.mp3", coub.UnfinishedPath, coub.No)
		err := os.Remove(soundPath)
		if err != nil {
			coub.Error(fmt.Sprintf("can't move sound file %v", soundPath))
		}
	}()
	coub.FFmpegRun(true, true, false, fmt.Sprintf("-i %v/%v.mp3 -shortest", coub.UnfinishedPath, coub.No))
	coub.Info("sounded")

	coub.nextState()
	return nil
}

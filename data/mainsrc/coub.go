package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"

	"golang.org/x/net/html"
)

// CoubStates is posible states for coubs
var CoubStates = []string{
	"Null",
	"Downloaded",
	"FFmpeg_cropped",
	"FFmpeg_scaled",
	"FFmpeg_backgrounded",
	"FFmpeg_multiplied",
	"FFmpeg_sounded",
}

// Coub is representation of coub video used in Clip type
type Coub struct {
	No             uint64  // Coub number in clip
	RawDuration    float64 // Raw coub duration
	Duration       float64 // Coub duration after multiply
	ID             string  // Coub ID name
	Title          string  // Coub title
	Music          string  // Not empty if coub has got music (composition title - author)
	RawWidth       uint64  // Coub video width after crop
	RawHeight      uint64  // Coub video height after crop
	ScaledWidth    uint64  // Coub video width after scaling
	ScaledHeight   uint64  // Coub video height after scaling
	State          string  // Current state of coub
	UnfinishedPath string  // Current coub unfinished files directory
}

// getClassValue get value from class names like musicTitle or musicAuthor
func getClassValue(n *html.Node, str string, res *string) {
	if n.Type == html.ElementNode && n.Data == "h3" {
		for _, a := range n.Attr {
			if a.Key == "class" && a.Val == str {
				*res = strings.TrimFunc(n.FirstChild.Data, func(r rune) bool {
					return r == '\n'
				})
			}
		}
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		getClassValue(c, str, res)
	}
}

// getLink get all versions of mp3 or mp4 download link for given coub or other class names like musicTitle
func getLink(n *html.Node, str string, res *string) {
	for _, a := range n.Attr {
		if a.Key == "class" && a.Val == str {
			*res = n.Attr[0].Val
		}
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		getLink(c, str, res)
	}
}

func cropToUint64(crop string) (uint64, uint64) {
	splitCropSize := strings.Split(crop, ":")
	w, errw := strconv.ParseInt(splitCropSize[0], 10, 64)
	if errw != nil {
		Error("can't convert crop width to uint64")
	}
	h, errh := strconv.ParseInt(splitCropSize[1], 10, 64)
	if errh != nil {
		Error("can't convert crop height to uint64")
	}
	return uint64(w), uint64(h)
}

// SaveFile get any file from given link and save it to target
func SaveFile(target string, link string) error {
	// Create empty file
	out, err := os.Create(target)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get data
	resp, err := http.Get(link)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Write the data to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}
	return nil
}

// Info show info messages with coub information prefix
func (coub *Coub) Info(v ...interface{}) {
	InfoL(3, fmt.Sprintf("[Coub #%v, state: %v]", coub.No, coub.State), strings.TrimSuffix(fmt.Sprintln(v...), "\n"))
}

// Error show error messages with coub information prefix
func (coub *Coub) Error(v ...interface{}) {
	ErrorL(3, fmt.Sprintf("[Coub #%v, state: %v]", coub.No, coub.State), strings.TrimSuffix(fmt.Sprintln(v...), "\n"))
}

// getFile find link of mp3 of mp4 coub part, download and save it
func (coub *Coub) getFile(n *html.Node, link *string, attribute string, format string, wg *sync.WaitGroup) {
	defer wg.Done()
	getLink(n, attribute, link)

	if *link == "" {
		coub.Error("can't find download link", n.Data)
	}

	if format == "mp4" {
		*link = savieoURL + *link
	}
	coub.Info(fmt.Sprintf("downloading %v", *link))

	target := fmt.Sprintf("%v/%v.%v", coub.UnfinishedPath, coub.No, format)
	err := SaveFile(target, *link)
	if err != nil {
		coub.Error(err)
	}
}

// nextState shift coub to next state
func (coub *Coub) nextState() {
	newState, err := NextState(coub.State, CoubStates)
	if err != nil {
		coub.Error(err)
	}
	oldState := coub.State
	coub.State = newState
	coub.Info(fmt.Sprintf("change state %v -> %v", oldState, coub.State))
}

func (coub *Coub) addMusicDescription(client *http.Client, wg *sync.WaitGroup) {
	defer wg.Done()
	if coub.ID == "" {
		coub.Error(fmt.Sprintf("empty ID in %#v coub", coub))
	}

	// Get coub page
	req, err := http.NewRequest("GET", coubURL+"/view/"+coub.ID, nil)
	if err != nil {
		coub.Error(err)
	}

	q := req.URL.Query()
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		coub.Error(err)
	}
	if resp.StatusCode != 200 {
		coub.Error(fmt.Sprintf("bad status code while downloading [status code = %v]", resp.StatusCode))
	}
	defer resp.Body.Close()

	// Find and filter coubs on page
	root, err := html.Parse(resp.Body)
	if err != nil {
		coub.Error(err)
	}

	// Set music title and author if they exist
	var musicTitle, musicAuthor, musicRes string
	musicWg := new(sync.WaitGroup)

	musicWg.Add(1)
	go func() {
		getClassValue(root, "musicTitle", &musicTitle)
		musicWg.Done()
	}()
	musicWg.Add(1)
	go func() {
		getClassValue(root, "musicAuthor", &musicAuthor)
		musicWg.Done()
	}()
	musicWg.Wait()

	if musicAuthor != "" && musicTitle != "" {
		musicRes = musicAuthor + " - " + musicTitle
	} else {
		musicRes = musicAuthor + musicTitle
	}

	if musicRes != "" {
		coub.Info("music title is", musicRes)
	}

	coub.Music = musicRes
}

// download clip by ID
func (coub *Coub) download(client *http.Client) {
	if coub.State != "Null" {
		return
	}
	if coub.ID == "" {
		coub.Error("empty ID")
	}

	// Get direct video and audio links for given coub
	req, err := http.NewRequest("GET", savieoDownloadURL, nil)
	if err != nil {
		coub.Error(err)
	}

	q := req.URL.Query()
	q.Add("url", coubURL+"/view/"+coub.ID)
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		coub.Error(err)
	}
	if resp.StatusCode != 200 {
		coub.Error(fmt.Sprintf("bad status code while downloading [status code = %v]", resp.StatusCode))
	}
	defer resp.Body.Close()

	// Find and filter coubs on page
	root, err := html.Parse(resp.Body)
	if err != nil {
		coub.Error(err)
	}

	// Download coub music as mp3 and video as mp4
	var mp3link, mp4link string
	wg := new(sync.WaitGroup)

	wg.Add(1)
	go coub.getFile(root, &mp3link, "download__link download__link--smaller download__link--with-filesize", "mp3", wg)
	wg.Add(1)
	go coub.getFile(root, &mp4link, "download__link download__link--smaller", "mp4", wg)
	wg.Wait()
	coub.Info("downloaded mp3 and mp4")

	coub.nextState()
}

func (coub *Coub) doFFmpeg(config *Config) {
	err := coub.ffmpegCrop()
	if err != nil {
		coub.Error(err)
	}
	err = coub.ffmpegScale(config.ClipsWidth, config.ClipsHeight)
	if err != nil {
		coub.Error(err)
	}

	err = coub.ffmpegBackground(config.ClipsWidth, config.ClipsHeight)
	if err != nil {
		coub.Error(err)
	}

	err = coub.ffmpegMultiply(config.OneCoubSecLength)
	if err != nil {
		coub.Error(err)
	}

	err = coub.ffmpegSound()
	if err != nil {
		coub.Error(err)
	}
}

// DoCoub start or continue coub processing
func (coub *Coub) DoCoub(config *Config, client *http.Client, wg *sync.WaitGroup) error {
	defer wg.Done()

	// Setup music description
	wg.Add(1)
	go coub.addMusicDescription(client, wg)

	coub.download(client)
	coub.doFFmpeg(config)

	return nil
}

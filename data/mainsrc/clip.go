package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"os/exec"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/stretchr/objx"
)

// ClipStates is posible states for clips
var ClipStates = []string{
	"Null",
	"Coubs_in_progress",
	"Coubs_done",
	"Clip_done",
	"Uploaded",
}

// Clip is representation of final clip used in YoutubeChannel type
type Clip struct {
	Title       string    // Clip title
	No          uint64    // Clip number
	Timestamp   time.Time // Creation time
	PreviewPath string    // Thumbnail clip image
	Coubs       []Coub    // Used coubs in this clip
	Description string    // Contains list of coubs description
	Tags        string    // Contains list of tags
	State       string    // Current state of clip
}

func filterCoub(
	c objx.Map,
	ch chan objx.Map,
	wg *sync.WaitGroup,
	config *Config,
	usedCoudsID *[]string) {

	defer wg.Done()

	// Ban by tags
	if c["age_restricted"] == true ||
		c["age_restricted_by_admin"] == true ||
		c["not_safe_for_work"] == true ||
		c["banned"] == true ||
		c["video_block_banned"] == true {
		return
	}

	// Ban anime
	rawjson := strings.ToLower(c.MustJSON())
	if strings.Contains(rawjson, "anime") {
		return
	}

	// Ban too old coubs
	createtime, ok := c["created_at"].(string)
	if !ok {
		Error("created_at is not a string")
	}
	t, err := time.Parse(time.RFC3339, createtime)
	if err != nil {
		Error(err)
	}
	if uint64(t.Sub(time.Now()).Seconds()/-86400) > config.DaysDepth {
		return
	}

	// Ban already used coubs
	for _, uc := range *usedCoudsID {
		if c["permalink"] == uc {
			return
		}
	}

	// Ban not funny
	if config.BanNotFunny {
		funny := false
		cats := c.Get("categories").MustObjxMapSlice()
		for _, cat := range cats {
			if cat["title"].(string) == "Funny" {
				funny = true
				break
			}
		}
		if !funny {
			return
		}
	}

	// Add filtered coub
	ch <- c
}

func resizeVideo(videoPath string, wTarget, hTarget uint64, wg *sync.WaitGroup, limit chan struct{}) {
	if videoPath != "" {
		wg.Add(1)
		go func(gwg *sync.WaitGroup) {
			limit <- struct{}{}
			_, _, err := FFmpegScale(videoPath, wTarget, hTarget, 99)
			if err != nil {
				Error(err)
			}
			wg.Done()
			<-limit
		}(wg)
	}
}

// Info show info messages with clip information prefix
func (clip *Clip) Info(v ...interface{}) {
	InfoL(3, fmt.Sprintf("[Clip #%v, state: %v]", clip.No, clip.State), strings.TrimSuffix(fmt.Sprintln(v...), "\n"))
}

// Error show error messages with clip information prefix
func (clip *Clip) Error(v ...interface{}) {
	ErrorL(3, fmt.Sprintf("[Clip #%v, state: %v]", clip.No, clip.State), strings.TrimSuffix(fmt.Sprintln(v...), "\n"))
}

// nextState shift clip to next state
func (clip *Clip) nextState() {
	newState, err := NextState(clip.State, ClipStates)
	if err != nil {
		clip.Error(err)
	}
	oldState := clip.State
	clip.State = newState
	clip.Info(fmt.Sprintf("change state %v -> %v", oldState, clip.State))
}

// getPage get all filtered coubs from page
func (clip *Clip) getPage(
	client *http.Client,
	pageNum uint64,
	ch chan objx.Map,
	wg *sync.WaitGroup,
	config *Config,
	usedCoudsID *[]string) {

	defer wg.Done()

	// Get page json body
	req, err := http.NewRequest("GET", coubAPIURL, nil)
	if err != nil {
		Error(err)
	}

	q := req.URL.Query()
	q.Add("page", strconv.FormatInt(int64(pageNum), 10))
	q.Add("per_page", strconv.FormatInt(int64(perPage), 10))
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		Error(err)
	}

	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		Error(err)
	}

	// Find and filter coubs on page
	raw, err := objx.FromJSON(string(respBody))
	if err != nil {
		Error(err)
	}

	coubs := raw.Get("coubs").MustInterSlice()
	for _, v := range coubs {
		gv, ok := v.(objx.Map)
		if !ok {
			Error("objx.Map is not a coub struct")
		}
		wg.Add(1)
		go filterCoub(gv, ch, wg, config, usedCoudsID)
	}
}

func (clip *Clip) showCoubsStatus() string {
	res := "Coubs status:"
	statuses := map[string]uint64{}
	for _, c := range clip.Coubs {
		_, ok := statuses[c.State]
		if !ok {
			statuses[c.State] = 1
		} else {
			statuses[c.State]++
		}
	}
	for _, s := range CoubStates {
		v, ok := statuses[s]
		if !ok {
			continue
		}
		res += fmt.Sprintf(" [%v: %v]", s, v)
	}
	return res
}

func (clip *Clip) setThumbnail(config *Config, wg *sync.WaitGroup, thumpURL string) {
	defer wg.Done()

	target := fmt.Sprintf("%v/thumb.jpg", config.UnfinishedPath)
	err := SaveFile(target, thumpURL)
	if err != nil {
		Error(err)
	}

	out, err := BashRun(fmt.Sprintf("convert -quality 100 %v -geometry 1280x720^ "+
		"-gravity center -crop 1280x720+0+0 %v", target, target))
	if err != nil {
		Error(fmt.Sprintf("can't scale thumbnail image: %v %v", err, out))
	}

	out, err = BashRun(fmt.Sprintf(`convert -quality 100 %v -pointsize 165 -font '%v' -stroke "#000" -strokewidth 15 `+
		`-draw "gravity west text 50,-240 '%v'" -stroke "transparent" -fill "#ff5" `+
		`-draw "gravity west text 50,-240 '%v'" %v`, target, config.MainThumbFont, config.Name, config.Name, target))
	if err != nil {
		Error(fmt.Sprintf("can't draw name channel on image: %v %v", err, out))
	}

	out, err = BashRun(fmt.Sprintf(`convert -quality 100 %v -pointsize 165 -font '%v' -stroke "#000" -strokewidth 15 `+
		`-draw "gravity west text 50,260 '%v'" -stroke "transparent" -fill "#ff5" `+
		`-draw "gravity west text 50,260 '%v'" %v`, target, config.MainThumbFont, config.Slogan, config.Slogan, target))
	if err != nil {
		Error(fmt.Sprintf("can't draw slogan on image: %v %v", err, out))
	}

	out, err = BashRun(fmt.Sprintf(`convert -quality 100 %v -pointsize 210 -font '%v' -stroke "#000" -strokewidth 21 `+
		`-draw "gravity center text -400,0 '# %v'" -stroke "transparent" -fill "#ff5" `+
		`-draw "gravity center text -400,0 '# %v'" %v`, target, config.SecondaryThumbFont, clip.No, clip.No, target))
	if err != nil {
		Error(fmt.Sprintf("can't clip number on image: %v %v", err, out))
	}

	if config.Logo != "" {
		out, err := BashRun(fmt.Sprintf("convert -quality 100 %v "+
			"-geometry x460 %v", config.Logo, config.Logo))
		if err != nil {
			Error(fmt.Sprintf("can't scale logo image: %v %v", err, out))
		}
		out, err = BashRun(fmt.Sprintf("convert -quality 100 %v "+
			"%v -geometry +900+100 -composite %v", target, config.Logo, target))
		if err != nil {
			Error(fmt.Sprintf("can't composite logo image with thumbnail: %v %v", err, out))
		}
	}
}

// findCoubs find and download best coubs for given period
func (clip *Clip) findCoubs(config *Config, wg *sync.WaitGroup, usedCoudsID *[]string) error {
	tr := &http.Transport{
		MaxIdleConns:       100,
		IdleConnTimeout:    90 * time.Second,
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}

	// Limit the maximum number of simultaneously calculated coubs
	var limit = make(chan struct{}, 3)

	// Check start, end and padding videos resolutions
	resizeVideo(config.StartSaverVideo, config.ClipsWidth, config.ClipsHeight, wg, limit)
	resizeVideo(config.PaddingVideo, config.ClipsWidth, config.ClipsHeight, wg, limit)
	resizeVideo(config.EndSaverVideo, config.ClipsWidth, config.ClipsHeight, wg, limit)

	if clip.State == "Null" {
		// Channel for coubs as maps
		chcoubs := make(chan objx.Map)

		pagesWg := new(sync.WaitGroup)
		for i := uint64(1); i < maxCoubs/perPage+1; i++ {
			pagesWg.Add(1)
			go clip.getPage(client, i, chcoubs, pagesWg, config, usedCoudsID)
		}

		go func() {
			pagesWg.Wait()
			close(chcoubs)
		}()

		// Collect and sort all found coubs
		coubs := make([]objx.Map, 0, 0)
		for c := range chcoubs {
			coubs = append(coubs, c)
		}
		sort.Slice(coubs, func(i, j int) bool {
			ic, ok1 := coubs[i]["likes_count"].(int)
			jc, ok2 := coubs[j]["likes_count"].(int)
			if !ok1 || !ok2 {
				Error("likes_count is not int")
			}
			return ic > jc
		})

		clip.Info(fmt.Sprintf("Found coubs: %v, need %v", len(coubs), config.GetCoubs))
		// Fail if number of coubs is below than GetCoubs
		if uint64(len(coubs)) < config.GetCoubs {
			return errors.New("not enough coubs")
		}

		// Save only GetCoubs number of best coubs
		coubs = coubs[:config.GetCoubs]

		// Create tmp directory for coubs processing
		err := os.MkdirAll(config.UnfinishedPath, os.ModePerm)
		if err != nil {
			return err
		}

		// Generate thumbnail image
		wg.Add(1)
		go clip.setThumbnail(config, wg, coubs[0]["picture"].(string))

		for i := range coubs {
			newcoub := Coub{
				No:             uint64(i + 1),
				State:          "Null",
				UnfinishedPath: config.UnfinishedPath,
			}
			clip.Coubs = append(clip.Coubs, newcoub)
		}

		for i, c := range coubs {
			clip.Info(fmt.Sprintf("Coub #%v [%v] %v/view/%v %v", i+1, c["likes_count"], coubURL, c["permalink"], c["title"]))
			permalink, ok := c["permalink"].(string)
			if !ok {
				Error("permalink is not string")
			}
			clip.Coubs[i].ID = permalink

			title, ok := c["title"].(string)
			if !ok {
				Error("title is not string")
			}
			clip.Coubs[i].Title = title

			wg.Add(1)
			go func(gi int, gclient *http.Client, gwg *sync.WaitGroup) {
				limit <- struct{}{}
				clip.Coubs[gi].DoCoub(config, gclient, gwg)
				<-limit
			}(i, client, wg)
		}

		clip.nextState()
	} else if clip.State == "Coubs_in_progress" {
		for i := range clip.Coubs {
			wg.Add(1)
			go func(gi int, gclient *http.Client, gwg *sync.WaitGroup) {
				go clip.Coubs[gi].DoCoub(config, gclient, gwg)
				<-limit
			}(i, client, wg)
		}
	}

	return nil
}

// сoncatenateCoubs concatenate all coubs into clip with end-saver and padding between videos
func (clip *Clip) сoncatenateCoubs(config *Config) error {
	if clip.State != "Coubs_done" {
		return nil
	}
	if len(clip.Coubs) == 0 {
		return fmt.Errorf("can't concatenate zero coubs")
	}

	// Start making coubs description
	done := make(chan bool)
	go clip.setDescription(config, done)

	binary, err := exec.LookPath("ffmpeg")
	if err != nil {
		return (err)
	}

	var inputs string
	var inputslen uint64
	if config.StartSaverVideo != "" {
		inputs += fmt.Sprintf(" -i %v ", config.StartSaverVideo)
		inputslen++
	}

	var coubinputs []string
	for i := range clip.Coubs {
		coubinputs = append(coubinputs, fmt.Sprintf("-i %v/%v.mp4", config.UnfinishedPath, i+1))
	}

	if config.PaddingVideo != "" {
		inputs += strings.Join(coubinputs[:], fmt.Sprintf(" -i %v ", config.PaddingVideo))
		inputslen += uint64(len(clip.Coubs)*2 - 1)
	} else {
		inputs += strings.Join(coubinputs[:], " ")
	}

	if config.EndSaverVideo != "" {
		inputs += fmt.Sprintf(" -i %v ", config.EndSaverVideo)
		inputslen++
	}

	var movies string
	for i := uint64(0); i < inputslen; i++ {
		movies += fmt.Sprintf("[%v:v][%v:a]", i, i)
	}

	cmdRun := fmt.Sprintf("%v -y %v -filter_complex \"%v concat=n=%v:v=1:a=1 [v] [a]\" "+
		"-map \"[v]\" -map \"[a]\" %v/%v_clip.mp4", binary, inputs, movies, inputslen, config.UnfinishedPath, clip.No)
	out, err := BashRun(cmdRun)
	clip.Info(cmdRun)
	if err != nil {
		Error(out, err)
	}

	for i := range clip.Coubs {
		coubPath := fmt.Sprintf("%v/%v.mp4", config.UnfinishedPath, i+1)
		err := os.Remove(coubPath)
		if err != nil {
			return fmt.Errorf("can't move file %v", coubPath)
		}
	}

	// Wait until clip description will be ready
	<-done

	clip.nextState()
	return nil
}

func (clip *Clip) setDescription(config *Config, done chan bool) {
	if clip.State != "Coubs_done" {
		clip.Error("coubs not done yet")
	}

	var currentTiming, padDuration float64

	if config.StartSaverVideo != "" {
		d := FFprobeRun(config.StartSaverVideo, "-show_entries format=duration -v quiet -of csv=\"p=0\"")
		ssvDuration, err := strconv.ParseFloat(d, 64)
		if err != nil {
			clip.Error(err)
		}
		currentTiming = ssvDuration
	}

	if config.PaddingVideo != "" {
		d := FFprobeRun(config.PaddingVideo, "-show_entries format=duration -v quiet -of csv=\"p=0\"")
		var err error
		padDuration, err = strconv.ParseFloat(d, 64)
		if err != nil {
			clip.Error(err)
		}
	}

	for i := uint64(0); i < uint64(config.GetCoubs); i++ {
		clip.Description += fmt.Sprintf(
			"[%v:%02v] %v",
			uint64(math.Round(currentTiming))%3600/60,
			uint64(math.Round(currentTiming))%60,
			clip.Coubs[i].Title,
		)
		if clip.Coubs[i].Music != "" {
			clip.Description += " (" + clip.Coubs[i].Music + ")"
		}
		clip.Description += "\n"

		currentTiming += clip.Coubs[i].Duration + padDuration
	}

	clip.Info(fmt.Sprintf("Coubs description:\n%v", clip.Description))

	done <- true
	close(done)
}

func (clip *Clip) getCoubs(config *Config, usedCoudsID *[]string) error {
	switch clip.State {
	case "Null":
	case "Coubs_in_progress":
		clip.Info("Continue from Coubs_in_progress state")
	default:
		return nil
	}

	wg := new(sync.WaitGroup)
	err := clip.findCoubs(config, wg, usedCoudsID)
	if err != nil {
		return err
	}

	// Periodically show coubs processing status
	quit, show := make(chan bool), make(chan bool)
	go func() {
		for {
			select {
			case <-quit:
				return
			case <-show:
				clip.Info(clip.showCoubsStatus())
			}
		}
	}()

	go func() {
		for clip.State == "Coubs_in_progress" || clip.State == "Null" {
			show <- true
			time.Sleep(5 * time.Second)
		}
	}()

	// Stop showing coubs status when coubs will be done
	wg.Wait()
	quit <- true

	clip.nextState()
	return nil
}

// DoClip search and filter best coubs and add them to download queue
func (clip *Clip) DoClip(config *Config, usedCoudsID *[]string) error {
	err := clip.getCoubs(config, usedCoudsID)
	if err != nil {
		return err
	}

	err = clip.сoncatenateCoubs(config)
	if err != nil {
		return err
	}

	return nil
}

// Upload using youtube api to upload video with name, description and tags
func (clip *Clip) Upload(config *Config, usedCoudsID *[]string) error {
	if clip.State != "Clip_done" {
		return nil
	}

	// Save clip coubs ids in already used list
	for _, c := range clip.Coubs {
		*usedCoudsID = append(*usedCoudsID, c.ID)
	}
	clip.Info(fmt.Sprintf("Add uploaded coubs to UsedCoubsID list: %v", *usedCoudsID))

	UploadClip(
		config, clip,
		fmt.Sprintf("%v/%v_clip.mp4", config.UnfinishedPath, clip.No),
		fmt.Sprintf("%v/thumb.jpg", config.UnfinishedPath),
	)

	clip.nextState()
	return nil
}

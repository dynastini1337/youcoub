package main

// Config is current configuration created from file and/or command line arguments
type Config struct {
	Name               string
	Slogan             string
	OneCoubSecLength   uint64
	DaysDepth          uint64
	BanNotFunny        bool
	GetCoubs           uint64
	OrderBy            string
	SavesPath          string
	LogsPath           string
	UnfinishedPath     string
	CredentialsOAuth2  string
	CredentialsToken   string
	Logo               string
	ClipsTitlePattern  string
	ClipsWidth         uint64
	ClipsHeight        uint64
	ClipsDescription   string
	ClipsTags          string
	ClipsLanguage      string
	PaddingVideo       string
	StartSaverVideo    string
	EndSaverVideo      string
	MainThumbFont      string
	SecondaryThumbFont string
	EmailNotification  string
	StartClipNumber    uint64
}

const (
	coubURL                  = "https://coub.com"                // coub website
	coubAPIURL               = coubURL + "/api/v2/timeline/hot/" // coub API URL for search coubs, default is hot section timeline history
	savieoURL                = "http://savieo.com"               // savieo website
	savieoDownloadURL        = savieoURL + "/download"           // savieo coub download page
	maxCoubs          uint64 = 2000                              // maximum coub number to search before filter
	perPage           uint64 = 25                                // number of coubs per page
)

var (
	qualities = [...]string{"high", "med", "low"} // possible coub video qualities
)

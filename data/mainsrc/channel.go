package main

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// YoutubeChannel is representation of youtube channel content description, creds, etc
type YoutubeChannel struct {
	SavePath    string   // Path to channel save file
	Config      Config   // Current channel configuration
	UsedCoubsID []string // List of used coub ids
	Clips       []Clip   // Published clips on channel
}

// NextState return next states for Coub and Clip structs
func NextState(cs string, ss []string) (string, error) {
	for i, v := range ss {
		if v == cs {
			if i+1 >= len(ss) {
				return "", errors.New("no next state")
			}
			return ss[i+1], nil
		}
	}
	return "", errors.New("no next state")
}

// PrevState return previous states for Coub and Clip structs
func PrevState(cs string, ss []string) (string, error) {
	for i, v := range ss {
		if v == cs {
			if i-1 < 0 {
				return "", errors.New("no prev state")
			}
			return ss[i-1], nil
		}
	}
	return "", errors.New("no prev state")
}

// Save function saves YoutubeChannel struct into gob file
func (yc *YoutubeChannel) Save() {
	err := SaveChannel(yc.SavePath, *yc)
	if err != nil {
		Fatal(err)
	}
	Info(fmt.Sprintf("Saving %v [clips num: %v, config: %v]", yc.SavePath, len(yc.Clips), yc.Config.Name))
}

// NewClip creates new Clip for YoutubeChannel
func (yc *YoutubeChannel) NewClip() *Clip {
	var startNo uint64

	if yc.Clips != nil {
		startNo = yc.Clips[len(yc.Clips)-1].No + 1
	} else {
		startNo = yc.Config.StartClipNumber
	}

	Nc := Clip{
		Title:     strings.Replace(yc.Config.ClipsTitlePattern, "{NUMBER}", strconv.FormatInt(int64(startNo), 10), -1),
		No:        startNo,
		Timestamp: time.Now(),
		State:     ClipStates[0],
	}

	yc.Clips = append(yc.Clips, Nc)
	yc.Save()
	Info(fmt.Sprintf("Creating new clip: %#v", Nc))

	return &Nc
}

func (yc *YoutubeChannel) savedRun(clip *Clip, fn string, args ...interface{}) {
	inputs := make([]reflect.Value, len(args))
	for i := range args {
		inputs[i] = reflect.ValueOf(args[i])
	}
	f := reflect.ValueOf(clip).MethodByName(fn)
	res := f.Call(inputs)
	yc.Save()
	if res[0].Interface() != nil {
		Error(res[0])
	}
}

// Proceed start or continue clip making
func (yc *YoutubeChannel) Proceed() {
	clip := &yc.Clips[len(yc.Clips)-1]
	yc.savedRun(clip, "DoClip", &yc.Config, &yc.UsedCoubsID)
	yc.savedRun(clip, "Upload", &yc.Config, &yc.UsedCoubsID)
}

package main

import (
	"encoding/gob"
	"os"
	"path/filepath"
)

// SaveChannel save YoutubeChannel to file via Gob
func SaveChannel(path string, yc YoutubeChannel) error {
	err := os.MkdirAll(filepath.Dir(path), os.ModePerm)
	if err != nil {
		return err
	}
	file, err := os.Create(path)
	defer file.Close()
	if err != nil {
		return err
	}
	encoder := gob.NewEncoder(file)
	encoder.Encode(yc)
	return nil
}

// LoadChannel load YoutubeChannel from Gob file
func LoadChannel(path string, yc *YoutubeChannel) error {
	file, err := os.Open(path)
	defer file.Close()
	if err != nil {
		return err
	}

	decoder := gob.NewDecoder(file)
	err = decoder.Decode(yc)
	return nil
}

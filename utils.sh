# This file must be located in near install.sh
# Сontains logging functions

### FUNCTIONS ###
dbg() {
    local UTILS_DBG="[DEBUG   $(date +'%Y-%m-%d %H:%M:%S%z')]: $@"
    echo "$UTILS_DBG"
}

info() {
    local UTILS_MSG="[INFO    $(date +'%Y-%m-%d %H:%M:%S%z')]: $@"
    echo "$UTILS_MSG"
    echo "$UTILS_MSG" | logger -p user.info
}

wrn() {
    local UTILS_WRN="[WARNING $(date +'%Y-%m-%d %H:%M:%S%z')]: $@"
    echo "$UTILS_WRN" >&2
    echo "$UTILS_WRN" | logger -p user.warning
}

err() {
    local UTILS_ERR="[ERROR   $(date +'%Y-%m-%d %H:%M:%S%z')]: $@"
    echo "$UTILS_ERR" >&2
    echo "$UTILS_ERR" | logger -p user.error
    exit 1
}

trap_error() {
    local parent_lineno="$1"
    local message="$2"
    local code="${3:-1}"
    if [[ -n "$message" ]] ; then
        local UTILS_ERR="[ERROR   $(date +'%Y-%m-%d %H:%M:%S%z') on line ${parent_lineno}: ${message}]"
    else
        local UTILS_ERR="[ERROR   $(date +'%Y-%m-%d %H:%M:%S%z') on line ${parent_lineno}]"
    fi

    echo "$UTILS_ERR" >&2
    echo "$UTILS_ERR" | logger -p user.error

    exit "${code}"
}

# Change values in ini files
# 1 - Path to ini file
# 2 - Key
# 3 - Value
set_config_option() {
    sed -i -e "/^$2\ *=/ s/= .*/= $3/" $1
    dbg "$1: $2 -> $3"
}

# Change values in jinja2 files
# 1 - Path to jinja2 file
# 2 - Key
# 3 - Value
set_jinja2_option() {
    sed -i -e "s/{{ $2 }}/$3/g" $1
    dbg "$1: $2 -> $3"
}